package com.brainhackerz.jira;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.project.property.ProjectPropertyService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class ProjectServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(ProjectServlet.class);
    private static final String templatePath = "com.brainhackerz.jira.property-editor:property-editor-template";
    private static final String templateKey = "BrainHackerz.PropertyEditor.index";
    private static final String contextKey = "property-editor-resources";
    private static final String initKey = "property-editor-init";

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final PageBuilderService pageBuilderService;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ApplicationProperties applicationProperties;
    private final ProjectService projectService;
    private final PermissionManager permissionManager;
    private final ProjectPropertyService projectPropertyService;
    private final ObjectMapper objectMapper;

    public ProjectServlet(
            SoyTemplateRenderer soyTemplateRenderer, PageBuilderService pageBuilderService,
            JiraAuthenticationContext jiraAuthenticationContext, ApplicationProperties applicationProperties,
            ProjectService projectService, PermissionManager permissionManager,
            ProjectPropertyService projectPropertyService) {
        this.soyTemplateRenderer = requireNonNull(soyTemplateRenderer);
        this.pageBuilderService = requireNonNull(pageBuilderService);
        this.jiraAuthenticationContext = requireNonNull(jiraAuthenticationContext);
        this.applicationProperties = requireNonNull(applicationProperties);
        this.projectService = requireNonNull(projectService);
        this.permissionManager = requireNonNull(permissionManager);
        this.projectPropertyService = requireNonNull(projectPropertyService);
        this.objectMapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log.info("Begin doGet");

        String[] pathInfo = req.getPathInfo().split("/");
        String projectKey = pathInfo[1];

        Errors errors = new Errors();
        ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
        ProjectService.GetProjectResult findProject = projectService.getProjectByKey(loggedInUser, projectKey.toUpperCase(Locale.ROOT));
        if (!findProject.isValid()) {
            errors.addJiraErrors(findProject.getErrorCollection());
            return;
        }

        Project project = findProject.get();
        List<EntityProperty> properties = projectPropertyService.getProperties(loggedInUser, project.getId());

        WebResourceAssembler assembler = pageBuilderService.assembler();
        assembler.resources().requireContext(contextKey);

        Map<String, Object> renderContext = new HashMap<>();

        renderContext.put("baseUrl", applicationProperties.getString(APKeys.JIRA_BASEURL));
        renderContext.put("username", loggedInUser.getUsername());
        renderContext.put("userKey", loggedInUser.getKey());
        renderContext.put("projectId", project.getId());
        renderContext.put("projectKey", project.getKey());
        renderContext.put("projectName", project.getName());
        renderContext.put("properties", properties);

        soyTemplateRenderer.render(resp.getWriter(), templatePath, templateKey, renderContext);

        log.info("Complete doGet");
    }

    @JsonAutoDetect
    private static class Errors {
        private final Set<Reason> reasons;
        private final List<String> errorMessages;
        private final Map<String, String> fieldErrors;

        private Errors() {
            reasons = new HashSet<>();
            errorMessages = new ArrayList<>();
            fieldErrors = new HashMap<>();
        }

        private void addJiraErrors(ErrorCollection jiraErrors) {
            errorMessages.addAll(jiraErrors.getErrorMessages());
            fieldErrors.putAll(jiraErrors.getErrors());
            reasons.addAll(jiraErrors.getReasons().stream().map(r -> Reason.find(r.getHttpStatusCode())).collect(Collectors.toSet()));
        }

        private List<String> getErrorMessages() {
            return errorMessages;
        }

        private void addErrorMessage(String errorMessage, Reason reason) {
            errorMessages.add(errorMessage);
            reasons.add(reason);
        }

        private Map<String, String> getFieldErrors() {
            return fieldErrors;
        }

        private void addError(String fieldName, String errorMessage, Reason reason) {
            fieldErrors.put(fieldName, errorMessage);
            reasons.add(reason);
        }

        private Set<Reason> getReasons() {
            return reasons;
        }
    }

    @JsonAutoDetect
    private enum Reason {
        BAD_REQUEST(400),
        NOT_FOUND(404),
        SERVER_ERROR(500);

        private static Reason worst(Set<Reason> set) {
            if (set == null) {
                return SERVER_ERROR;
            } else if (set.contains(NOT_FOUND)) {
                return NOT_FOUND;
            } else if (set.contains(BAD_REQUEST)) {
                return BAD_REQUEST;
            } else {
                return SERVER_ERROR;
            }
        }

        private static Reason find(Integer code) {
            if (code == null)
                return SERVER_ERROR;

            switch (code) {
                case 400:
                    return BAD_REQUEST;
                case 404:
                    return NOT_FOUND;
                case 500:
                default:
                    return SERVER_ERROR;
            }
        }

        private final Integer code;

        Reason(Integer code) {
            this.code = code;
        }

        public Integer getCode() {
            return code;
        }
    }
}
