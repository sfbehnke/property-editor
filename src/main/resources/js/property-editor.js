(function ($, window, document, AJS) {
    window.BrainHackerz = window.BrainHackerz || {};
    BrainHackerz.PropertyEditor = BrainHackerz.PropertyEditor || {};

    AJS.toInit(function () {
        _init_meta();

        $('.property-trigger-delete').on('click', function () {
            const button = this;
            button.busy();

            const $button = $(button);
            const clickedKey = $button.attr('data-key');
            _get_property(clickedKey).done(function(data) {
                const auiDialog = _aui_dialog(
                    'property-delete-dialog',
                    'small',
                    true,
                    false,
                    'Delete an existing property',
                    '<input type="hidden" class="hidden" id="property-key" value="' + data.key + '" /><p>This will permanently remove <strong>' + data.key + '</strong> from your project.</p>',
                    'Delete',
                    'Cancel'
                );
                auiDialog.show();

                const $auiDialog = $('#property-delete-dialog');
                $auiDialog.find('.aui-dialog2-header-close .aui-icon, .aui-dialog2-footer-actions .dialog-cancel').on('click', function () {
                    auiDialog.hide();
                });
                auiDialog.on('hide', function (){
                    button.idle();
                });

                $auiDialog.find('.aui-dialog2-footer-actions .dialog-submit').on('click', function () {
                    const submitButton = this;
                    const keyInput = document.getElementById('property-key');
                    submitButton.busy();

                    _delete_property(keyInput.value).statusCode({
                        204: function () {
                            $('tr[data-key="' + keyInput.value + '"]').remove();
                            const flag = AJS.flag({
                                type: 'success',
                                close: 'manual',
                                title: 'Deleted successfully'
                            });
                            setTimeout(function () {
                                flag.close();
                            }, 60000);
                        },
                        400: _handle_400
                    }).done(function () {
                        submitButton.setAttribute('disabled', 'disabled');
                        setTimeout(function () {
                            auiDialog.hide();
                            button.idle();
                        }, 2500);
                    }).always(function () {
                        setTimeout(function () {
                            submitButton.idle();
                        }, 1000);
                    });
                });
            });
        });

        $('.property-trigger-update').on('click', function () {
            const button = this;
            button.busy();

            const $button = $(button);
            const clickedKey = $button.attr('data-key');
            _get_property(clickedKey).done(function(data) {
                const auiDialog = _aui_dialog(
                    'property-update-dialog',
                    'medium',
                    false,
                    false,
                    'Update an existing property',
                    _property_form(data.key, JSON.stringify(data.value)),
                    'Update',
                    'Cancel'
                );
                auiDialog.show();

                const $auiDialog = $('#property-update-dialog');
                $auiDialog.find('.aui-dialog2-header-close .aui-icon, .aui-dialog2-footer-actions .dialog-cancel').on('click', function () {
                    auiDialog.hide();
                });
                auiDialog.on('hide', function (){
                    button.idle();
                });

                $auiDialog.find('.aui-dialog2-footer-actions .dialog-submit').on('click', function () {
                    const that = this;
                    const property = _parse_form_for_property(that);
                    _set_up_dialog(property, auiDialog, that, button);
                });
            });
        });

        $('.property-create-trigger').on('click', function () {
            const button = this;
            button.busy();
            const auiDialog = _aui_dialog(
                'property-create-dialog',
                'medium',
                false,
                false,
                'Create a new property',
                _property_form(),
                'Create',
                'Cancel'
            );
            auiDialog.show();

            const $auiDialog = $('#property-create-dialog');
            $auiDialog.find('.aui-dialog2-header-close .aui-icon, .aui-dialog2-footer-actions .dialog-cancel').on('click', function () {
                auiDialog.hide();
            });
            auiDialog.on('hide', function () {
                button.idle();
            });

            $auiDialog.find('.aui-dialog2-footer-actions .dialog-submit').on('click', function () {
                const that = this;
                const property = _parse_form_for_property(that);
                _set_up_dialog(property, auiDialog, that, button);
            });
        });
    });

    function _set_up_dialog(property, dialog, formButton, tableButton) {
        _put_property(property.key, property.value).statusCode({
            200: function () {
                _get_property(property.key).done(function(data) {
                    $('tr[data-key="' + data.key + '"]').replaceWith(BrainHackerz.PropertyEditor.row({key: data.key, value: JSON.stringify(data.value)}));
                });
                const flag = AJS.flag({
                    type: 'success',
                    close: 'manual',
                    title: 'Updated successfully'
                });
                setTimeout(function () {
                    flag.close();
                }, 60000);
            },
            201: function () {
                _get_property(property.key).done(function(data) {
                    $(BrainHackerz.PropertyEditor.row({key: data.key, value: JSON.stringify(data.value)})).prependTo('table#project-properties > tbody');
                });
                const flag = AJS.flag({
                    type: 'success',
                    close: 'manual',
                    title: 'Created successfully'
                });
                setTimeout(function () {
                    flag.close();
                }, 6000);
            },
            400: _handle_400
        }).done(function () {
            formButton.setAttribute('disabled', 'disabled');
            setTimeout(function () {
                dialog.hide();
                tableButton.idle();
            }, 2500);
        }).always(function () {
            setTimeout(function () {
                formButton.idle();
            }, 1000);
        });
    }

    function _handle_400(data) {
        data['responseJSON']['errorMessages'].forEach(function (errorMessage) {
            AJS.messages.error('#update-property .error-messages', {
                title: 'Error while creating property',
                body: '<p>' + errorMessage + '</p>',
                closeable: true,
            });
        });
    }

    function _url(key) {
        return (BrainHackerz.PropertyEditor.meta.baseUrl + '/rest/api/2/project/' +
            BrainHackerz.PropertyEditor.meta.projectId + '/properties/' + key);
    }

    function _put_property(propertyKey, propertyValue) {
        return _ajax({
            url: _url(propertyKey),
            method: 'put',
            data: JSON.stringify(propertyValue)
        });
    }

    function _delete_property(propertyKey) {
        return _ajax({
            url: _url(propertyKey),
            method: 'delete'
        });
    }

    function _get_property(propertyKey) {
        return _ajax({
            url: _url(propertyKey),
            method: 'get'
        });
    }

    function _parse_form_for_property(submitButton) {
        const keyInput = document.getElementById('property-key');
        const valueInput = document.getElementById('property-value');
        const form = document.getElementById('update-property');

        valueInput.setCustomValidity('');
        submitButton.busy();

        if (!form.reportValidity()) {
            submitButton.idle();
            return;
        }

        const property = _parse_values(keyInput.value, valueInput.value);
        if (property.error) {
            valueInput.setCustomValidity(property.error);
            valueInput.reportValidity();

            submitButton.idle();
            return;
        }

        return property;
    }

    function _parse_values(key, value) {
        const result = {};
        result.error = undefined;

        if (!key || key.trim().length === 0) {
            result.key = undefined;
        } else {
            result.key = key.trim();
        }

        if (value.trim().length === 0) {
            result.value = undefined;
        } else {
            try {
                result.value = JSON.parse(value);
            } catch (e) {
                result.value = undefined;
                result.error = e;
            }
        }

        return result;
    }

    const _default_ajax_options = {
        contentType: 'application/json',
        dataType: 'json',
        processData: false
    };

    function _ajax(options_override) {
        return $.ajax({
            ..._default_ajax_options,
            ...options_override
        });
    }

    /**
     * Returns a string from a meta attribute
     *
     * @param name String a name of the attribute to get
     * @returns String
     */
    function _get_meta_attribute(name) {
        return $('[name="' + ('property-editor-' + name) + '"]').attr('content');
    }

    /**
     * Inflates a helper object
     */
    function _init_meta() {
        BrainHackerz.PropertyEditor.meta = {
            baseUrl: _get_meta_attribute('baseurl'),
            projectId: _get_meta_attribute('projectid'),
            projectKey: _get_meta_attribute('projectkey'),
            projectName: _get_meta_attribute('projectname'),
            username: _get_meta_attribute('username'),
            userKey: _get_meta_attribute('userkey')
        };
    }

    /**
     * AUI Dialog 2
     * https://aui.atlassian.com/aui/latest/docs/dialog2.html
     *
     * @param id String used for the dialog id
     * @param size String of sizes small medium large xlarge
     * @param warning Boolean of whether to show red banner
     * @param modal Boolean of whether to prevent closure without answer
     * @param header String simple header text
     * @param body String html text
     * @param primary String optional button text
     * @param cancel String optional button text
     *
     * @returns dialog
     */
    function _aui_dialog(id, size, warning, modal, header, body, primary, cancel) {
        return AJS.dialog2(BrainHackerz.PropertyEditor.dialog({
            id: id,
            size: size,
            warning: warning,
            modal: modal,
            header: header,
            body: body,
            primary: primary,
            cancel: cancel
        }));
    }

    function _property_form(key, value) {
        return BrainHackerz.PropertyEditor.form({
            key: key,
            value: value
        });
    }
})(AJS.$, window, document, AJS);
